<%@ taglib prefix="spring" uri="http://www.springframework.org/tags/form" %>
<%--
  Created by IntelliJ IDEA.
  User: tasha
  Date: 15.11.17
  Time: 20:29
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Users</title>
</head>
<body>
  <spring:form method="post"
               modelAttribute="userFromServer"
               action="/web-context/users/sayCongrats">
      Login <spring:input path="login"/> <br>
      Password <spring:input path="password"/><br>
      Email <spring:input path="email"/><br>
      <spring:button>SUBMIT</spring:button>
  </spring:form>
</body>
</html>
