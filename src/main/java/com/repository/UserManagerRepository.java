package com.repository;


import com.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository("userManagerRepository")
@Transactional
public class UserManagerRepository {
    @Autowired
    private UserRepository userRepository;


    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    public UserRepository getUserRepository() {
        return userRepository;
    }

    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public void printUsersByLogin(String login) {
        List<User> UserList = userRepository.findByLogin(login);
        for (User User : UserList) {
            System.out.println(User);
        }
    }
//
//    public void printAllUserInfo2() {
//        List<User> UserList = userRepository.findAll();
//        for (User User : UserList) {
//            System.out.println(User);
//        }
//    }


}
