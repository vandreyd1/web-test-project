package com.model;

/*

придумайте систему валидации данных юзера(не обязательно только логин и пароль),
которая в случае некорректного ввода/указания значений для юзера ххх  - будет выбрасывать ошибку
(включите в него наши кастомные ошибки) + разбор сессии по РЕСТ АПИ

*/

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "USER", schema = "PUBLIC", catalog = "TEST_USERS_DB")
public class User implements Serializable {
    private int id;
    private String login;
    private String password;
    private String email;

    public User() {
    }

    public User(String login, String password, String email) {
        this.login = login;
        this.password = password;
        this.email = email;
    }

    public User(String login, String password) {
        this.login = login;
        this.password = password;
    }

    //
//    public User(int id, String login, String password, String email) {
//        this.id = id;
//        this.login = login;
//        this.password = password;
//        this.email = email;
//    }

    @Id @GeneratedValue
    @Column(name = "ID")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "LOGIN")
    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    @Basic
    @Column(name = "PASSWORD")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Basic
    @Column(name = "EMAIL")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        if (!login.equals(user.login)) return false;
        return password.equals(user.password);
    }

    @Override
    public int hashCode() {
        int result = login.hashCode();
        result = 31 * result + password.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
