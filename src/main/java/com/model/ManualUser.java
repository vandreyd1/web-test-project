package com.model;

/*

придумайте систему валидации данных юзера(не обязательно только логин и пароль),
которая в случае некорректного ввода/указания значений для юзера ххх  - будет выбрасывать ошибку
(включите в него наши кастомные ошибки) + разбор сессии по РЕСТ АПИ

*/

import javax.persistence.*;


public class ManualUser {
    private int id;
    private String login;
    private String password;
    private String email;

    public ManualUser() {
    }

    public int getId() {
        return id;
    }

    public ManualUser(String login) {
        this.login = login;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLogin() {
        return this.login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ManualUser user = (ManualUser) o;

        if (id != user.id) return false;
        if (!login.equals(user.login)) return false;
        if (!password.equals(user.password)) return false;
        return email != null ? email.equals(user.email) : user.email == null;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + login.hashCode();
        result = 31 * result + password.hashCode();
        result = 31 * result + (email != null ? email.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
